OBJDIR2=$(OBJDIR)obj
CFLAGS2=$(CFLAGS)
CFLAGS2+=-Wall -Wuninitialized -Wno-unused -s -O2

BINNAME=game

debug: OBJDIR2+=dbg
debug: CFLAGS2+=-g
debug: BINNAME+=_dbg

ifeq ($(origin CC),default)
	ifeq ($(OS),Windows_NT)
		CC = gcc
	endif
endif

C_FILES=$(wildcard src/*.c)
OBJECTS=$(patsubst src/%,$(OBJDIR2)/%,$(patsubst %.c,%.o,$(C_FILES)))

HAVE_LIBS=

INCLUDE_PATHS=
LINK_PATHS=

ifeq ($(OS),Windows_NT)
	INCLUDE_PATHS+=-I$(ALLEGRO_PATH)include
	LINK_PATHS+=-L$(ALLEGRO_PATH)lib
endif

ifeq ($(OS),Windows_NT)
	ifneq ($(NO_PTHREADS),1)
		INCLUDE_PATHS+=-I$(PTHW32_PATH)include
		LINK_PATHS+=-L$(PTHW32_PATH)lib
		HAVE_LIBS+=-lpthreadGC2
	else
		CFLAGS2+=-DNO_PTHREADS
	endif
endif

HEADER_FILES=$(wildcard src/*.h)

$(OBJDIR2)/%.o: src/%.c $(HEADER_FILES)
	$(CC) $(INCLUDE_PATHS) $(CFLAGS2) $< -c -o $@

clean:
	rm -f $(OBJDIR2)/src/*.o
	rm -f *.exe

$(BINNAME).exe: $(OBJECTS)
	$(CC) $(LINK_PATHS) $(LINK_FLAGS) $(CFLAGS2) $(OBJECTS) -o $(BINDIR)$(BINNAME).exe $(HAVE_LIBS) -lalleg -lm

regular: $(BINNAME).exe

debug: $(BINNAME).exe
