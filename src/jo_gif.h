#ifndef JO_GIF_H
#define JO_GIF_H

#include <stdio.h>

typedef struct {
	FILE *fp;
	unsigned char palette[0x300];
	short width, height, repeat;
	int numColors, palSize;
	int frame;
	unsigned char *buf[2];
	void *palkdn, *palkdt;
	int dither;
	void (*gamma_func)(unsigned char *, int);
} jo_gif_t;

// width/height	| the same for every frame
// repeat       | 0 = loop forever, 1 = loop once, etc...
// palSize		| must be power of 2 - 1. so, 255 not 256.
int jo_gif_start(jo_gif_t *, const char *filename, short width, short height, short repeat, int palSize);

// gif			| the state (returned from jo_gif_start)
// rgba         | the pixels
// delayCsec    | amount of time in between frames (in centiseconds)
// localPalette | true if you want a unique palette generated for this frame (does not effect future frames)
void jo_gif_frame(jo_gif_t *gif, unsigned char *rgba, short delayCsec, int localPalette);

// gif          | the state (returned from jo_gif_start)
void jo_gif_end(jo_gif_t *gif);

#endif
