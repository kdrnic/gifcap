#ifndef GIFCAP_H
#define GIFCAP_H

#include "jo_gif.h"

#ifndef NO_PTHREADS
#include <pthread.h>
#endif

struct gifcap_t;

//Internal stuff
typedef struct gifcap_thr_arg_t
{
	struct gifcap_t *gc;
	unsigned char *frame_data;
} gifcap_thr_arg_t;

typedef struct gifcap_t
{
	//Don't alter those directly
	jo_gif_t jo;
	int delta, delta_error;
	#ifndef NO_PTHREADS
	pthread_mutex_t thr_lock;
	pthread_cond_t thr_cond;
	pthread_t thr;
	gifcap_thr_arg_t thr_arg;
	int thr_has_frame;
	#endif
	void (*frameb_func)(unsigned char *, int);
	int gen_pal;
	int block;
} gifcap_t;

//GIF capture params
typedef struct gifcap_param_t
{
	short width, height;
	//0 - loop forever, 1 play once and don't repeat, 2 repeat once, 3 repeat twice and so on
	short repeat;
	//palette size - 0 to 256, 256 won't allow delta frames (causing huge animated gifs)
	int num_colours;
	int fps;
	//0 - no dither, 1 - floyd-steinberg error diffusion dithering
	int dither;
	//if not null, a single fixed palette to be used by the entire gif
	unsigned char *fixed_palette;
	//if set, won't multithread, gifcap_frame will block for encoding and file output done synchronously
	int blocking;
	//will be called for every palette, if not null
	//for e.g.: gamma correct dithering, set to gamma encoding function
	void (*pal_func)(unsigned char *, int);
	//will be called for every frame buffer, if not null
	//for e.g.: gamma correct dithering, set to gamma decoding function;
	//          colour components ordering conversion
	void (*frameb_func)(unsigned char *, int);
} gifcap_param_t;

//returns 0 on success, non-0 on failure
int gifcap_start(gifcap_t *cap, const char *filename, gifcap_param_t params);
//buf has {b,g,r,x,...} entries, where x is unused
void gifcap_frame(gifcap_t *cap, unsigned char *buf);
void gifcap_end(gifcap_t *cap);

#endif
