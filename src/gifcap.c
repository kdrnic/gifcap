#include "gifcap.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#ifndef NO_PTHREADS
#define GIF_FUNC_LOOP 1
#else
#define GIF_FUNC_LOOP 0
#endif

static void *gif_func(void *arg)
{
	unsigned char *c;
	gifcap_thr_arg_t *gft = arg;
	gifcap_t *gc = gft->gc;
	
	do{
		#ifndef NO_PTHREADS
		pthread_mutex_lock(&gc->thr_lock);
		while(!gc->thr_has_frame){
			pthread_cond_wait(&gc->thr_cond, &gc->thr_lock);
		}
		#endif
		
		c = gft->frame_data;
		if(gc->frameb_func) gc->frameb_func(c, gc->jo.width * gc->jo.height * 4);
		
		gc->delta_error += gc->delta % 100;
		jo_gif_frame(&gc->jo, c, (gc->delta + gc->delta_error) / 100, gc->gen_pal);
		gc->gen_pal = 0;
		gc->delta_error = gc->delta_error % 100;
		
		#ifndef NO_PTHREADS
		gc->thr_has_frame = 0;
		pthread_mutex_unlock(&gc->thr_lock);
		#endif
	} while(GIF_FUNC_LOOP);
	
	return 0;
}

int gifcap_start(gifcap_t *cap, const char *filename, gifcap_param_t params)
{
	if(jo_gif_start(&cap->jo, filename, params.width, params.height, params.repeat, params.num_colours))
		return 1;
	if(params.fixed_palette)
		memcpy(
			cap->jo.palette + 3 * (params.num_colours != 256),
			params.fixed_palette,
			params.num_colours * 3
		);
	cap->gen_pal = !params.fixed_palette;
	cap->jo.dither = params.dither;
	cap->jo.gamma_func = params.pal_func;
	cap->frameb_func = params.frameb_func;
	cap->delta = 10000 / params.fps;
	cap->block = params.blocking;
	#ifndef NO_PTHREADS
	if(!params.blocking)
	{
		cap->thr_has_frame = 0;
		cap->thr_cond = PTHREAD_COND_INITIALIZER;
		cap->thr_lock = PTHREAD_MUTEX_INITIALIZER;
		cap->thr_arg.gc = cap;
		if(pthread_create(&cap->thr, 0, gif_func, &cap->thr_arg)){
			jo_gif_end(&cap->jo);
			return 1;
		}
		cap->thr_arg.frame_data = malloc(params.width * params.height * 4);
	}
	#endif
	return 0;
}

void gifcap_frame(gifcap_t *cap, unsigned char *buf)
{
	#ifndef NO_PTHREADS
	if(!cap->block){
		pthread_mutex_lock(&cap->thr_lock);
		memcpy(cap->thr_arg.frame_data, buf, cap->jo.width * cap->jo.height * 4);
		cap->thr_has_frame = 1;
		pthread_cond_signal(&cap->thr_cond);
		pthread_mutex_unlock(&cap->thr_lock);
	}
	else
	#endif
	{
		gifcap_thr_arg_t arg = {cap, buf};
		gif_func(&arg);
	}
}

void gifcap_end(gifcap_t *cap)
{
	#ifndef NO_PTHREADS
	pthread_mutex_lock(&cap->thr_lock);
	free(cap->thr_arg.frame_data);
	pthread_cancel(cap->thr);
	#endif
	jo_gif_end(&cap->jo);
}
