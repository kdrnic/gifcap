#include <allegro.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <math.h>
#include <locale.h>
#include <errno.h>

#include "gifcap.h"

static void xrgb_to_bgrx(unsigned char *b, int siz)
{
	int i;
	unsigned int *B = (unsigned int *) b;
	for(i = 0; i < siz / 4; i++) B[i] = __builtin_bswap32(B[i]) >> 8;
}

double freqR, freqG, freqB;
double phaseR, phaseG, phaseB;

void draw_mandelbrot(BITMAP *bmp, double centerX, double centerY, double radius)
{
	/* screen ( integer) coordinate */
	int iX,iY;
	const int iXmax = bmp->w; 
	const int iYmax = bmp->h;
	/* world ( double) coordinate = parameter plane*/
	double Cx,Cy;
	const double CxMin= centerX - radius;
	const double CxMax= centerX + radius;
	const double CyMin= centerY - radius * (double) bmp->h / (double) bmp->w;
	const double CyMax= centerY + radius * (double) bmp->h / (double) bmp->w;;
	/* */
	const double PixelWidth=(CxMax-CxMin)/iXmax;
	//double PixelHeight=(CyMax-CyMin)/iYmax;
	const double PixelHeight = PixelWidth;
	/* color component ( R or G or B) is coded from 0 to 255 */
	/* it is 24 bit color RGB file */
	const int MaxColorComponentValue=255; 
	unsigned char color[3];
	/* Z=Zx+Zy*i  ;   Z0 = 0 */
	double Zx, Zy;
	double Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
	/*  */
	int Iteration;
	const int IterationMax=200;
	/* bail-out value , radius of circle ;  */
	const double EscapeRadius=2;
	const double ER2=EscapeRadius*EscapeRadius;
	/* compute and write image data bytes to the file*/
	for(iY=0;iY<iYmax;iY++){
		Cy=CyMin + iY*PixelHeight;
		if(fabs(Cy)< PixelHeight/2) Cy=0.0; /* Main antenna */
		for(iX=0;iX<iXmax;iX++){
			Cx=CxMin + iX*PixelWidth;
			/* initial value of orbit = critical point Z= 0 */
			Zx=0.0;
			Zy=0.0;
			Zx2=Zx*Zx;
			Zy2=Zy*Zy;
			/* */
			for(Iteration=0;Iteration<IterationMax && ((Zx2+Zy2)<ER2);Iteration++){
				Zy=2*Zx*Zy + Cy;
				Zx=Zx2-Zy2 +Cx;
				Zx2=Zx*Zx;
				Zy2=Zy*Zy;
			};
			/* compute  pixel color (24 bit = 3 bytes) */
			if(Iteration==IterationMax){
				/*  interior of Mandelbrot set = black */
				color[0]=0;
				color[1]=0;
				color[2]=0;
			}
			else{ /* exterior of Mandelbrot set = white */
				double mandelColor = (double) Iteration / (double) IterationMax;
				color[0]=255 * (sin((mandelColor + phaseR) * freqR) + 1.0) * 0.5;
				color[1]=255 * (sin((mandelColor + phaseG) * freqG) + 1.0) * 0.5;
				color[2]=255 * (sin((mandelColor + phaseB) * freqB) + 1.0) * 0.5;
			}
			putpixel(bmp, iX, iY, makecol(color[0], color[1], color[2]));
		}
	}
}

static double linterp(double s, double e, double i)
{
	return s + (e - s) * i;
}

int main(int argc, char **argv)
{
	allegro_init();
	set_color_depth(32);
	set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0);
	
	srand(time(0));
	freqR = ((double) rand() / (double) RAND_MAX) * 30.0;
	freqG = ((double) rand() / (double) RAND_MAX) * 30.0;
	freqB = ((double) rand() / (double) RAND_MAX) * 30.0;
	phaseR = ((double) rand() / (double) RAND_MAX) * 2.0 * 3.141592653;
	phaseG = ((double) rand() / (double) RAND_MAX) * 2.0 * 3.141592653;
	phaseB = ((double) rand() / (double) RAND_MAX) * 2.0 * 3.141592653;
	
	BITMAP *gif_bmp = create_bitmap(200, 150);
	
	gifcap_t gif_cap = {};
	gifcap_param_t gif_params = {
		.width = gif_bmp->w,
		.height = gif_bmp->h,
		.repeat = 0,
		.num_colours = 128,
		.fps = 60,
		.dither = 1,
		.fixed_palette = 0,
		.blocking = 0,
		.pal_func = 0,
		.frameb_func = xrgb_to_bgrx
	};
	
	gifcap_start(&gif_cap, "mandelbrot.gif", gif_params);
	
	const double x0 = -1.0, y0 = 0.0, x1 = -1.31, y1 = 0.0, r0 = 0.25, r1 = 0.06;
	double time = 0.0;
	
	while(time <= 1.0){
		clear(gif_bmp);
		
		time += 1.0 / (double) gif_params.fps;
		
		const double x = linterp(x0, x1, time);
		const double y = linterp(y0, y1, time);
		const double r = linterp(r0, r1, time);
		
		draw_mandelbrot(gif_bmp, x, y, r);
		gifcap_frame(&gif_cap, gif_bmp->line[0]);
		
		stretch_blit(gif_bmp, screen, 0, 0, gif_bmp->w, gif_bmp->h, 0, 0, SCREEN_W, SCREEN_H);
	}
	
	gifcap_end(&gif_cap);
	
	return 0;
}
END_OF_MAIN()
