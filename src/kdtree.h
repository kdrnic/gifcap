#ifndef KDTREE_H
#define KDTREE_H

struct kd_node_t
{
	int x[3];
	struct kd_node_t *left, *right;
	int idx;
};

struct kd_node_t *kdtree_make_tree(struct kd_node_t *t, int len, int i);
void kdtree_nearest(
	const struct kd_node_t *root, const struct kd_node_t *nd, int i,
	struct kd_node_t const **best, int *best_dist
);

#endif
