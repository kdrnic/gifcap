#include <string.h>

#include "kdtree.h"

inline static int dist(const struct kd_node_t *a, const struct kd_node_t *b, int dim)
{
	int t, d = 0;
	while(dim--){
		t = a->x[dim] - b->x[dim];
		d += t * t;
	}
	return d;
}

inline static void swap(struct kd_node_t *x, struct kd_node_t *y)
{
	int tmp[3];
	int tmpidx;
	memcpy(tmp,  x->x, sizeof(tmp));
	memcpy(x->x, y->x, sizeof(tmp));
	memcpy(y->x, tmp,  sizeof(tmp));
	tmpidx = x->idx;
	x->idx = y->idx;
	y->idx = tmpidx;
}

static struct kd_node_t* kdtree_find_median(struct kd_node_t *start, struct kd_node_t *end, int idx)
{
	if(end <= start) return 0;
	if(end == start + 1) return start;
 
	struct kd_node_t *p, *store, *md = start + (end - start) / 2;
	int pivot;
	while(1){
		pivot = md->x[idx];
 
		swap(md, end - 1);
		for(store = p = start; p < end; p++){
			if(p->x[idx] < pivot){
				if(p != store) swap(p, store);
				store++;
			}
		}
		swap(store, end - 1);
		
		if(store->x[idx] == md->x[idx]) return md;
 
		if(store > md) end = store;
		else start = store;
	}
}

struct kd_node_t *kdtree_make_tree(struct kd_node_t *t, int len, int i)
{
	struct kd_node_t *n;
	const int dim = 3;
	
	if(!len) return 0;
	
	if((n = kdtree_find_median(t, t + len, i))){
		i = (i + 1) % dim;
		n->left  = kdtree_make_tree(t, n - t, i);
		n->right = kdtree_make_tree(n + 1, t + len - (n + 1), i);
	}
	return n;
}

void kdtree_nearest(
	const struct kd_node_t *root, const struct kd_node_t *nd, int i,
	struct kd_node_t const **best, int *best_dist
)
{
	int d, dx, dx2;
	const int dim = 3;
	
	if(!root) return;
	d = dist(root, nd, dim);
	dx = root->x[i] - nd->x[i];
	dx2 = dx * dx;
	
	if(!*best || d < *best_dist){
		*best_dist = d;
		*best = root;
	}
	
	if(*best_dist < 16) return;
	
	if(++i >= dim) i = 0;
	
	kdtree_nearest(dx > 0 ? root->left : root->right, nd, i, best, best_dist);
	if(dx2 >= *best_dist) return;
	kdtree_nearest(dx > 0 ? root->right : root->left, nd, i, best, best_dist);
}
