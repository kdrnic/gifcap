# KDRNIC's gifcap library #

This repository hosts kdrnic's gifcap library

Read more about it [on my blog post here](https://kdrnic.github.io/index_gifcap.html)

Public-domain licensed

Fundamentally based on [Jon Olick's gifcap library](https://www.jonolick.com/home/gif-writer)

Uses Allegro 4 for the demo example in main.c which is a mandelbrot set renderer

# Improvements upon Jon Olick's original #

* Multithreading
* Faster palette search with kd-tree
* Delta-frames for smaller gifs
* C rather than C++
